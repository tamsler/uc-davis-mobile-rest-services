package edu.ucdavis.mobile.rest.service;

import java.util.List;

public class PageableResult<T> {

	private List<T> results;
	private String offset = "";
	
	public PageableResult() {
		
	}
	
	public List<T> getResults() {
		return results;
	}
	
	public void setResults(List<T> results) {
		this.results = results;
	}
	
	public String getOffset() {
		return offset;
	}
	
	public void setOffset(String offset) {
		this.offset = offset;
	}
	
}
