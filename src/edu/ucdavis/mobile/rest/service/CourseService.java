package edu.ucdavis.mobile.rest.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.apache.log4j.Logger;
import org.datanucleus.store.appengine.query.JDOCursorHelper;

import com.google.appengine.api.datastore.Cursor;

import edu.ucdavis.mobile.rest.pojo.Course;

public class CourseService {
	
	private static final Logger log = Logger.getLogger(CourseService.class);

	private static final String ZERO_OFFSET = "0";
	private static final int ZERO = 0;
	private static final int MAX_LIMIT = 15;
	private static final int BATCH_CREATE_SIZE = 1000;
	
	public CourseService() { }

	public void create(Course course) {

		PersistenceManager persistenceManager = PMF.get().getPersistenceManager();

		try {

			persistenceManager.makePersistent(course);
		}
		finally {

			persistenceManager.close();
		}
	}

	public void createAll(List<Course> courses) {

		PersistenceManager persistenceManager = PMF.get().getPersistenceManager();

		try {

			persistenceManager.makePersistentAll(courses);
		}
		finally {

			persistenceManager.close();
		}
	}
	
	public PageableResult<Course> get(String offset, long limit) {

		PersistenceManager persistenceManager = PMF.get().getPersistenceManager();
		
		PageableResult<Course> pageableResult = new PageableResult<Course>();
		
		Query query = persistenceManager.newQuery(Course.class);
		
		if(null != offset && !"".equals(offset) && !ZERO_OFFSET.equals(offset)) {
			
			Cursor cursor = Cursor.fromWebSafeString(offset);
			Map<String, Cursor> extensionMap = new HashMap<String, Cursor>();
			extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
			query.setExtensions(extensionMap);
		}

		if(MAX_LIMIT < limit) {

			limit = MAX_LIMIT;
		}
		else if(ZERO >= limit) {

			limit = MAX_LIMIT;
		}

		query.setRange(ZERO, limit);

		List<Course> courses = null;

		try {

			courses = (List<Course>) query.execute();

			if(courses.size() != ZERO) {
				
				Cursor cursor = JDOCursorHelper.getCursor(courses);
				pageableResult.setOffset(cursor.toWebSafeString());
			}
			
			persistenceManager.makeTransientAll(courses);
		}
		finally {

			persistenceManager.close();
		}

		pageableResult.setResults(courses);
		return pageableResult;
	}

	public void deleteAll() {

		PersistenceManager persistenceManager = PMF.get().getPersistenceManager();

		try {

			log.info("#### Delete all: START");
			Query query = persistenceManager.newQuery(Course.class);
			query.deletePersistentAll();
			log.info("#### Delete all: END");
		}
		finally {

			persistenceManager.close();
		}
	}

	public PageableResult<Course> findBySubject(String subjectCode, String offset, long limit) {

		PageableResult<Course> pageableResult = new PageableResult<Course>();
		
		if(null == subjectCode || subjectCode.equals("")) {
			return pageableResult;
		}
		
		PersistenceManager persistenceManager = PMF.get().getPersistenceManager();
		
		Query query = persistenceManager.newQuery(Course.class, "subject.startsWith(:subject)");

		if(null != offset && !"".equals(offset) && !ZERO_OFFSET.equals(offset)) {

			Cursor cursor = Cursor.fromWebSafeString(offset);
			Map<String, Cursor> extensionMap = new HashMap<String, Cursor>();
			extensionMap.put(JDOCursorHelper.CURSOR_EXTENSION, cursor);
			query.setExtensions(extensionMap);
		}

		if(MAX_LIMIT < limit) {
			
			limit = MAX_LIMIT;
		}
		else if(ZERO >= limit) {
			
			limit = MAX_LIMIT;
		}
		
		query.setRange(ZERO, limit);

		List<Course> courses = null;

		try {

			courses = (List<Course>) query.execute(subjectCode.toUpperCase());
			
			if(courses.size() != ZERO) {
			
				Cursor cursor = JDOCursorHelper.getCursor(courses);
				pageableResult.setOffset(cursor.toWebSafeString());
			}

			persistenceManager.makeTransientAll(courses);
		}
		finally {

			persistenceManager.close();
		}

		pageableResult.setResults(courses);
		return pageableResult;
	}

	public void importAll() {

		boolean startProcessing = false;
		int batchCount = 0;
		List<Course> batchCreateList = new ArrayList<Course>();

		log.info("#### Import Courses: START");
		long start = System.currentTimeMillis();
		try {

			URL url = new URL("http://registrar4.ucdavis.edu/courses/search/cl_iphone_multi_term.cfm");
			URLConnection urlConnection = url.openConnection();
			urlConnection.setConnectTimeout(300000);
			
			log.info("#### Import Courses: Start Getting Data ...");
			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			long mid = System.currentTimeMillis();
			log.info("#### Import Courses: Received Data : time = " + (mid - start) / 1000 + " sec");
			

			String line;
			Course course = null;
			boolean canCreate = false;
			int counter = 0;

			while ((line = reader.readLine()) != null) {

				if(startProcessing) {

					String [] tokens = line.trim().split("\\s\\s+");

					if(tokens.length == 11) {

						if(canCreate) {

							counter++;
							batchCount++;
							batchCreateList.add(course);
						}

						course = new Course();
						
						course.setCrn(tokens[0]);
						
						course.setSubject(tokens[1]);
						
						course.setTitle(tokens[2]);
						
						List<String> daysAndTime = new ArrayList<String>();
						daysAndTime.add(tokens[3] + "," + tokens[4]);
						course.setDaysAndTime(daysAndTime);
						
						List<String> room = new ArrayList<String>();
						room.add(tokens[5]);
						course.setRoom(room);
						
						course.setOpenSeats(tokens[6]);
						
						course.setDropStatus(tokens[7]);
						
						course.setInstructor(tokens[8]);
						
						course.setTerm(tokens[9]);
						
						course.setMaxEnrollment(tokens[10]);
						
						canCreate = true;
					}
					else if(tokens.length == 5) {

						course.getDaysAndTime().add(tokens[0] + "," + tokens[1]);
						course.getRoom().add(tokens[2]);
						canCreate = false;
						counter++;
						batchCount++;
						batchCreateList.add(course);
					}
				}

				if(line.startsWith("<B>CRN")) {
					startProcessing = true;
				}
				
				if(batchCount > BATCH_CREATE_SIZE) {
					createAll(batchCreateList);
					batchCreateList.clear();
					batchCount = 0;
				}
			}
			
			createAll(batchCreateList);
			reader.close();
			long end = System.currentTimeMillis();
			log.info("#### Import Courses: END");
			log.info("#### Import Courses: TIME = " + (end - start) / 1000 + " sec");
			log.info("#### Import Courses: COUNT = " + counter);

		} catch (MalformedURLException e) {
			log.error("MalformedURLException", e);
		} catch (IOException e) {
			log.error("IOException", e);
		} catch (Exception e) {
			log.error("Exception", e);
		}
	}

}
