package edu.ucdavis.mobile.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class OverQuotaException implements ExceptionMapper<com.google.apphosting.api.ApiProxy.OverQuotaException> {

	@Override
	public Response toResponse(com.google.apphosting.api.ApiProxy.OverQuotaException exception) {

		return Response.status(404).entity(exception.getMessage()).type("text/plain").build();
	}
}
