package edu.ucdavis.mobile.rest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.sun.jersey.spi.resource.Singleton;

import edu.ucdavis.mobile.rest.pojo.Course;
import edu.ucdavis.mobile.rest.service.CourseService;
import edu.ucdavis.mobile.rest.service.PageableResult;


@Singleton
@Path("/courses")
public class CoursesResource {

	private CourseService courseService = new CourseService();

	@GET
	@Path("/all/")
	@Produces("application/json")
	public PageableResult<Course> getCourses(
			@QueryParam("offset") String offset,
			@QueryParam("limit")  long limit) {

		return courseService.get(offset, limit);
	}

	@GET
	@Path("/find/")
	@Produces("application/json")
	public PageableResult<Course> find(
			@QueryParam("subject") String subject,
			@QueryParam("offset") String offset,
			@QueryParam("limit") long limit) {

		return courseService.findBySubject(subject, offset, limit);
	}
	
	@DELETE
	@Path("/cron/delete")
	public void cronDeleteAll() {

		courseService.deleteAll();
	}
	
	@GET
	@Path("/task/delete")
	@Produces("text/plain")
	public String taskDeleteAll() {
		
		Queue queue = QueueFactory.getDefaultQueue();
	    queue.add(TaskOptions.Builder.withUrl("/api/courses/cron/delete").method(Method.DELETE));
	    return "OK";
	}
	
	@POST
	@Path("/cron/import")
	public void cronImportCourses() {

		courseService.importAll();
	}
	
	@GET
	@Path("/task/import")
	@Produces("text/plain")
	public String taskImportCourses() {
		
		Queue queue = QueueFactory.getDefaultQueue();
	    queue.add(TaskOptions.Builder.withUrl("/api/courses/cron/import").method(Method.POST));
	    return "OK";
	}

}
