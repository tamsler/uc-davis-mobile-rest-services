package edu.ucdavis.mobile.rest.pojo;

import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(detachable = "true")
public class Course {

	@PrimaryKey
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    private Long id;
	
	@Persistent
	private String crn;
	
	@Persistent
	private String subject;
	
	@Persistent
	private String title;
	
	@Persistent
	private List<String> daysAndTime;

	@Persistent
	private List<String> room;
	
	@Persistent
	private String openSeats;
	
	@Persistent
	private String dropStatus;
	
	@Persistent
	private String instructor;
	
	@Persistent
	private String term;
	
	@Persistent
	private String maxEnrollment;
	
	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getDaysAndTime() {
		return daysAndTime;
	}

	public void setDaysAndTime(List<String> daysAndTime) {
		this.daysAndTime = daysAndTime;
	}

	public List<String> getRoom() {
		return room;
	}

	public void setRoom(List<String> room) {
		this.room = room;
	}

	public String getOpenSeats() {
		return openSeats;
	}

	public void setOpenSeats(String openSeats) {
		this.openSeats = openSeats;
	}

	public String getDropStatus() {
		return dropStatus;
	}

	public void setDropStatus(String dropStatus) {
		this.dropStatus = dropStatus;
	}

	public String getInstructor() {
		return instructor;
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getMaxEnrollment() {
		return maxEnrollment;
	}

	public void setMaxEnrollment(String maxEnrollment) {
		this.maxEnrollment = maxEnrollment;
	}
}
